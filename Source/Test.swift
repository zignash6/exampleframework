//
//  Test.swift
//  Example1
//
//  Created by Undrakonda Gopala Krishna on 09/02/21.
//

import Foundation

public class Test {
    
    public static let shared = Test()
    
    public func doSomething() {
        print("Do something....")
    }
    
}
