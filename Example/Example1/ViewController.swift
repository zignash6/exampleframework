//
//  ViewController.swift
//  Example1
//
//  Created by undrakonda_krishna on 02/09/2021.
//  Copyright (c) 2021 undrakonda_krishna. All rights reserved.
//

import UIKit
import Example1

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        Test.shared.doSomething()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

