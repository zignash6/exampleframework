//
//  PMAppInfo.h
//  PlayMetrics
//
//  Created by Vishal Singh Panwar on 23/10/17.
//  Copyright © 2017 GVC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMBaseJSONModel.h"

/**
 Contains basic app information.   
 */
@interface PMAppInfo : PMBaseJSONModel

/**
 Host app version.
 */
@property(nonatomic, copy)NSString *appVersion;
/**
 :nodoc:
 */
@property(nonatomic, copy)NSString *appVersionInt;
/**
 Host app name.
 */
@property(nonatomic, copy)NSString *appName;

+(instancetype)defaultAppInfo;
@end
