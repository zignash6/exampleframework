//
//  PMPokerAppEvent.h
//  PlayMetrics
//
//  Created by Vishal Singh Panwar on 26/10/17.
//  Copyright © 2017 GVC. All rights reserved.
//

#import <PlayMetrics/PlayMetrics.h>

typedef NS_ENUM(NSUInteger, PMPokerEvent) {
    PMPokerEvent_NONE,
    PMPokerEvent_BROWSE,
    PMPokerEvent_DEPOSIT,
    PMPokerEvent_INIT,
    PMPokerEvent_LOG_IN,
    PMPokerEvent_OPEN,
    PMPokerEvent_PLAYED_TABLE,
    PMPokerEvent_REGISTRATION,
    PMPokerEvent_RE_BUY,
    PMPokerEvent_TOURNAMENT,
    PMPokerEvent_CLICKED,
    PMPokerEvent_UPDATE_COUNTRY,
    PMPokerEvent_UPDATE_FCM,
    PMPokerEvent_UPDATE_STATE,
    PMPokerEvent_UNREGISTER
};

typedef NS_ENUM(NSUInteger, PMPokerPlayProductType) {
    PMPokerPlayProductType_NONE,
    PMPokerPlayProductType_SLOTS,
    PMPokerPlayProductType_BLACKJACK,
    PMPokerPlayProductType_ROULETTE,
    PMPokerPlayProductType_POKER,
    PMPokerPlayProductType_OTHER
};

typedef NS_ENUM(NSUInteger, PMPokerPlayProductCategory) {
    PMPokerPlayProductCategory_NONE,
    PMPokerPlayProductCategory_CASH,
    PMPokerPlayProductCategory_TOURNAMENT
};

typedef NS_ENUM(NSUInteger, PMPokerDepositState) {
    PMPokerDepositState_NONE,
    PMPokerDepositState_C
};

/**
 Contains details of the events specific to Poker app to register with PlayMetrics.
 */
@interface PMPokerAppEvent : PMAppEvent

@property(nonatomic, copy)NSString *pokerBrowseCategory;
@property(nonatomic, copy)NSString *pokerBrowseProductType;
@property(nonatomic, copy)NSString *pokerBrowseProductTitle;
@property(nonatomic, copy)NSString *pokerBrowsePlayType;

@property(nonatomic, copy)NSString *pokerDepositAmount;
@property(nonatomic, copy)NSString *pokerDepositCurrency;

@property(nonatomic, copy)NSString *pokerPlayedProductTitle;

@property(nonatomic, copy)NSString *pokerRegUserId;
@property(nonatomic, copy)NSString *pokerRegUserCategory;
@property(nonatomic, copy)NSString *pokerRegUserChannel;

@property(nonatomic, copy)NSString *pokerReBuyAmount;
@property(nonatomic, copy)NSString *pokerReBuyCurrency;

@property(nonatomic, copy)NSString *pokerTourneyBuyInAmount;
@property(nonatomic, copy)NSString *pokerTourneyId;
@property(nonatomic, copy)NSString *pokerLogin;

@property(nonatomic, assign)PMPokerPlayProductType playProductType;
@property(nonatomic, assign)PMPokerPlayProductCategory playProductCategory;
@property(nonatomic, assign)PMPokerDepositState depositState;


/**
 :nodoc:
 */
- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithEventType:(PMPokerEvent)eventType NS_DESIGNATED_INITIALIZER;
@end
