# Example1

[![CI Status](https://img.shields.io/travis/undrakonda_krishna/Example1.svg?style=flat)](https://travis-ci.org/undrakonda_krishna/Example1)
[![Version](https://img.shields.io/cocoapods/v/Example1.svg?style=flat)](https://cocoapods.org/pods/Example1)
[![License](https://img.shields.io/cocoapods/l/Example1.svg?style=flat)](https://cocoapods.org/pods/Example1)
[![Platform](https://img.shields.io/cocoapods/p/Example1.svg?style=flat)](https://cocoapods.org/pods/Example1)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Example1 is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Example1'
```

## Author

undrakonda_krishna, undrakonda.krishna@ivycomptech.com

## License

Example1 is available under the MIT license. See the LICENSE file for more info.
