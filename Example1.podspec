#
# Be sure to run `pod lib lint Example1.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Example1'
  s.version          = '1.0.0'
  s.summary          = 'Example1 is testing for cocoapods..'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  Example1 is testing to cocoapods....
                       DESC

  s.homepage         = 'https://zignash@bitbucket.org/zignash6/exampleframework'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'undrakonda_krishna' => 'undrakonda.krishna@ivycomptech.com' }
  s.source           = { :git => 'https://zignash@bitbucket.org/zignash6/exampleframework.git', :tag => s.version.to_s }
  

  s.ios.deployment_target = '12.0'

  s.source_files = 'Source/**/*.{h,m,swift}'
  s.swift_version = '5.0'
  s.platforms = {
      "ios": "12.0"
  }
  # s.resource_bundles = {
  #   'Example1' => ['Example1/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'Firebase/Analytics'
  s.dependency 'Firebase/Crashlytics'
  
end
